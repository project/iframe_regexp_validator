<?php

namespace Drupal\iframe_regexp_validator\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Field user input must respect the regular expression of the field config.
 */
class IframeRegExpUriConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   *
   * Validate if URL is respecting the regular expression from field conf.
   */
  public function validate($value, Constraint $constraint) {
    $field_list = $value->getValue();
    foreach ($field_list as $field) {
      $url_field = $field['url'] ?? FALSE;
      if ($url_field && !empty($constraint->regexp)) {
        if (!preg_match($constraint->regexp, $url_field)) {
          $this->context->addViolation($constraint->errorMessage, [
            '%value' =>
            $url_field,
            '%regexp' => $constraint->regexp,
          ]);
        }
      }
    }
  }

}
