<?php

namespace Drupal\iframe_regexp_validator\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Iframe field must respect the regular expression of the field config.
 *
 * @Constraint(
 *   id = "IframeRegExpUri",
 *   label = @Translation("User input in the field must respect the regular expression of the field configuration.", context = "Validation"),
 * )
 */
class IframeRegExpUriConstraint extends Constraint {
  /**
   * Invalid format on url iframe, doesn't match the regexp field config.
   *
   * @var string
   */
  public string $errorMessage = '"%value" is not respecting the regular expression "%regexp".';

  /**
   * Regular expression from the iframe field configuration.
   *
   * @var string
   */
  public string $regexp;

}
